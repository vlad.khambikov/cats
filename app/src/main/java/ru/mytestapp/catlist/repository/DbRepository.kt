package ru.mytestapp.catlist.repository

import ru.mytestapp.catlist.data.entities.CatFavorite

interface DbRepository {

    suspend fun getFavorites(): List<CatFavorite>

    suspend fun addFavorite(catFavorite: CatFavorite)

    suspend fun deleteFavorite(id: String)
}