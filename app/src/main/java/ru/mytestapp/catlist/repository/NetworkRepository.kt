package ru.mytestapp.catlist.repository

import ru.mytestapp.catlist.data.network.response.CatResponse

interface NetworkRepository {

    suspend fun getCats(page:Int = 0):List<CatResponse>
}