package ru.mytestapp.catlist.repository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.mytestapp.catlist.data.network.CatsApi
import ru.mytestapp.catlist.data.network.response.CatResponse

class NetworkRepositoryImpl(private val catsApi: CatsApi) : NetworkRepository {


    override suspend fun getCats(page: Int): List<CatResponse> {
        return withContext(Dispatchers.IO) {
            val response = catsApi.getCats(10, page)
            return@withContext withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    response.body() as List<CatResponse>
                } else {
                    emptyList()
                }
            }
        }
    }


}