package ru.mytestapp.catlist.repository

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.mytestapp.catlist.data.db.DataBaseRoom
import ru.mytestapp.catlist.data.entities.CatFavorite
import ru.mytestapp.catlist.data.network.response.CatResponse
import kotlin.coroutines.CoroutineContext

class DbRepositoryImpl(val database: DataBaseRoom) : DbRepository, CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    override suspend fun getFavorites(): List<CatFavorite> {
        return withContext(Dispatchers.IO) {
            database.favoriteDao().getFavorites()
        }
    }

    override suspend fun addFavorite(catFavorite: CatFavorite) {
        return withContext(Dispatchers.IO) {
            database.favoriteDao().insert(catFavorite)
        }
    }

    override suspend fun deleteFavorite(id: String) {
        return withContext(Dispatchers.IO) {
            database.favoriteDao().deleteFavoriteById(id)
        }
    }
}