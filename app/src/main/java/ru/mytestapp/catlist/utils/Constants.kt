package ru.mytestapp.catlist.utils

object Constants {

    /**
     * Константы для Базы данных
     */
    object DataBase {
        const val NO_ID: Long = 0L
    }

    /**
     * Константы для работы с картинками
     */
    object ImageConst {
        /**
         * Максимальный размер картинки
         */
        const val MAX_SIZE = 240

        /**
         * Путь где будет сохраняться картинка
         */
        const val DIR_NAME= "cats/"

    }
}