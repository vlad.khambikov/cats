package ru.mytestapp.catlist.data.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import ru.mytestapp.catlist.data.network.response.CatResponse

interface CatsApi {

    /**
     * live_yl3FSFezMgXio59gmXB37s6e56pvvfXyZ5NwTCPo9wqUmtsjhWBxZTChkCMWt0Yu
     */


    @GET("/v1/images/search")
    suspend fun getCats(
        @Query("limit") limits: Int,
        @Query("page") page: Int
    ): Response<List<CatResponse>>
}