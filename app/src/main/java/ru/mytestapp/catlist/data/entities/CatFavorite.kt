package ru.mytestapp.catlist.data.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import kotlinx.parcelize.Parcelize
import ru.mytestapp.todolist.data.converts.DateConverter
import ru.mytestapp.catlist.utils.Constants
import java.util.*

@TypeConverters(DateConverter::class)
@Parcelize
@Entity(tableName = "favorites")
data class CatFavorite(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id")
    var id: Long = Constants.DataBase.NO_ID,

    var catId: String = "",

    var name: String = "",
    var description: String = "",
    var height: Int = 0,
    var width: Int = 0,
    @ColumnInfo(name = "date_create")
    var dateCreate: Date = Date(),

    var path: String = ""
) : Parcelable