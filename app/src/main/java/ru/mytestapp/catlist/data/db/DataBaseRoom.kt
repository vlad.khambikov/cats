package ru.mytestapp.catlist.data.db

import android.content.Context
import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ru.mytestapp.catlist.data.dao.FavoriteDao
import ru.mytestapp.catlist.data.entities.CatFavorite


@Database(
    version = DataBaseRoom.VERSION,
    entities = [
        CatFavorite::class
    ],
    exportSchema = true,
    autoMigrations = [
        AutoMigration(from = 1, to = 2),
    ]
)
abstract class DataBaseRoom : RoomDatabase() {

    abstract fun favoriteDao(): FavoriteDao


    companion object {
        @JvmStatic
        fun getInstanceForDagger(context: Context): DataBaseRoom {
            val dbName = "cat_room.db"
            return Room
                .databaseBuilder(
                    context.applicationContext,
                    DataBaseRoom::class.java,
                    dbName
                )
                .build()
        }

        const val VERSION = 2
    }
}