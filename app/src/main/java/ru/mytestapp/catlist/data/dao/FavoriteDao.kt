package ru.mytestapp.catlist.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.mytestapp.catlist.data.entities.CatFavorite

@Dao
interface FavoriteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(catFavorite: CatFavorite)


    /**
     * Получить все избранные
     */
    @Query("SELECT * FROM favorites")
    suspend fun getFavorites(): List<CatFavorite>

    /**
     * Удалить избранное по id
     */
    @Query("DELETE FROM favorites WHERE catId = :id")
    suspend fun deleteFavoriteById(id: String)

}