package ru.mytestapp.catlist.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.mytestapp.catlist.di.scope.ActivityScope
import ru.mytestapp.catlist.di.scope.FragmentScope
import ru.mytestapp.catlist.ui.MainActivity
import ru.mytestapp.catlist.ui.fragment.CatsFragment


internal abstract class ScreenBuilder {

    @Module
    internal abstract inner class ActivityBuilder {
        @ContributesAndroidInjector()
        @ActivityScope
        abstract fun bindMainActivity(): MainActivity
    }

    @Module
    internal abstract inner class FragmentBuilder {

        @ContributesAndroidInjector()
        @FragmentScope
        abstract fun bindCatsFragment(): CatsFragment
    }
}