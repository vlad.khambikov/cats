package ru.mytestapp.catlist.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import ru.mytestapp.catlist.app.CatsApplication
import ru.mytestapp.catlist.di.modules.AppModule
import ru.mytestapp.catlist.di.modules.DataModule
import ru.mytestapp.catlist.di.modules.DomainModule
import ru.mytestapp.catlist.di.modules.NetworkCatsModule
import ru.mytestapp.catlist.di.modules.ScreenBuilder
import ru.mytestapp.catlist.ui.MainActivity
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        DataModule::class,
        DomainModule::class,
        NetworkCatsModule::class,
        AndroidInjectionModule::class,
        ScreenBuilder.ActivityBuilder::class,
        ScreenBuilder.FragmentBuilder::class
    ]
)
interface AppComponent : AndroidInjector<CatsApplication> {

    override fun inject(app: CatsApplication)
    fun inject(activity: MainActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun appModule(appModule: AppModule): Builder

        @BindsInstance
        fun dataModule(dataModule: DataModule): Builder

        @BindsInstance
        fun domainModule(domainModule: DomainModule): Builder

        @BindsInstance
        fun netWorkModule(netWorkModule: NetworkCatsModule): Builder

        fun build(): AppComponent
    }
}