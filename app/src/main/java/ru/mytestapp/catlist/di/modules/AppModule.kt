package ru.mytestapp.catlist.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import okhttp3.logging.HttpLoggingInterceptor
import ru.mytestapp.catlist.data.network.CatsApi
import ru.mytestapp.catlist.repository.DbRepository
import ru.mytestapp.catlist.repository.NetworkRepository
import ru.mytestapp.catlist.repository.NetworkRepositoryImpl
import ru.mytestapp.catlist.ui.MainViewModelFactory
import javax.inject.Singleton

@Module
class AppModule {
    private lateinit var _context: Context

    @Singleton
    @Provides
    fun provideContext(application: Application): Context {
        _context = application.applicationContext
        return _context
    }

    @Singleton
    @Provides
    fun provideMainViewModelFactory(
        dbRepository: DbRepository,
        networkRepository: NetworkRepository
    ): MainViewModelFactory =
        MainViewModelFactory(dbRepository, networkRepository)

    @Singleton
    @Provides
    fun provideNetworkRepository(catsApi: CatsApi): NetworkRepository =
        NetworkRepositoryImpl(catsApi)

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor =
            HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }
}