package ru.mytestapp.catlist.di.modules

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.mytestapp.catlist.data.network.CatsApi
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkCatsModule {

    @Provides
    @Singleton
    open fun provideApi(
        @Named("http_client") okHttpClient: OkHttpClient
    ): CatsApi =
        Retrofit.Builder()
            .baseUrl("https://api.thecatapi.com")
            .client(okHttpClient)
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder().serializeNulls().create()
                )
            )
            .build()
            .create(CatsApi::class.java)


    @Provides
    @Singleton
    @Named("http_client")
    fun getHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            .followRedirects(false)
            .addInterceptor(loggingInterceptor)
            .addInterceptor { chain ->
                val original = chain.request()
                val builder = original
                    .newBuilder()
                    .addHeader(
                        "x-api-key",
                        "live_yl3FSFezMgXio59gmXB37s6e56pvvfXyZ5NwTCPo9wqUmtsjhWBxZTChkCMWt0Yu"
                    )
                val request = builder.build()
                return@addInterceptor chain.proceed(request)
            }
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()
}