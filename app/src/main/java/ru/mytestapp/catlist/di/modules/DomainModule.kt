package ru.mytestapp.catlist.di.modules

import dagger.Module
import dagger.Provides
import ru.mytestapp.catlist.data.db.DataBaseRoom
import ru.mytestapp.catlist.repository.DbRepository
import ru.mytestapp.catlist.repository.DbRepositoryImpl
import javax.inject.Singleton

@Module
class DomainModule {

    @Singleton
    @Provides
    fun provideDbRepository(databaseRoom: DataBaseRoom): DbRepository =
        DbRepositoryImpl(databaseRoom)
}