package ru.mytestapp.catlist.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.mytestapp.catlist.data.db.DataBaseRoom
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    fun provideRoomDao(context: Context): DataBaseRoom =
        DataBaseRoom.getInstanceForDagger(context)
}