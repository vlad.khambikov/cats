package ru.mytestapp.catlist.ui.fragment

import ru.mytestapp.catlist.data.entities.CatFavorite

sealed class AdapterItem {
    data class Cats(
        val id: String,
        val imageUrl: String,
        val imageParams: ImageParams,
        var isFavorites: Boolean = false
    ) : AdapterItem() {

        companion object {
            fun Cats.mapToFavorites(): CatFavorite {
                return CatFavorite().apply {
                    catId = this@mapToFavorites.id
                    path = this@mapToFavorites.imageUrl
                    height = this@mapToFavorites.imageParams.height
                    width = this@mapToFavorites.imageParams.width
                }
            }
        }

    }

    object Load : AdapterItem()
}
