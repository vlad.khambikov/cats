package ru.mytestapp.catlist.ui.fragment

import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


abstract class PaginationListener(
    private val layoutManager: LinearLayoutManager,
    private val count: Int
) : RecyclerView.OnScrollListener() {

    /**
     * Событие на начало прокрутки списка
     *
     */
    protected abstract fun scrollStarted()

    /**
     * Последняя ли страница
     *
     */
    protected abstract fun isLastItem(): Boolean

    /**
     * Идет ли загрузка списка
     *
     */
    protected abstract fun isLoading(): Boolean

    /**
     * Загрузка следующей страницы
     *
     */
    protected abstract fun loadMoreItems()

    private var previousLastVisibleItemPosition: Int = 0

    override fun onScrolled(@NonNull recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (dy > 0 && layoutManager.findFirstVisibleItemPosition() == 0) {
            scrollStarted()
        }

        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
        val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()

        if (!isLoading() && !isLastItem()) {
            val needLoadMoreItems =
                previousLastVisibleItemPosition != lastVisibleItemPosition &&
                        visibleItemCount + firstVisibleItemPosition >= totalItemCount &&
                        firstVisibleItemPosition >= 0 &&
                        totalItemCount >= count &&
                        lastVisibleItemPosition == totalItemCount - 1
            if (needLoadMoreItems) {
                previousLastVisibleItemPosition = lastVisibleItemPosition
                loadMoreItems()
            }
        }
    }
}