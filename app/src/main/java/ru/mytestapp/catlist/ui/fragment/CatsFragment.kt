package ru.mytestapp.catlist.ui.fragment

import android.Manifest
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.support.DaggerFragment
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import ru.mytestapp.catlist.R
import ru.mytestapp.catlist.data.entities.CatFavorite
import ru.mytestapp.catlist.data.network.response.CatResponse
import ru.mytestapp.catlist.databinding.FragmentCatsListBinding
import ru.mytestapp.catlist.ui.MainViewModel
import ru.mytestapp.catlist.utils.Constants
import java.io.File
import kotlin.math.max
import kotlin.math.min

class CatsFragment : DaggerFragment() {

    companion object {
        private const val IS_FAVORITE_FRAGMENT = "IS_FAVORITE_FRAGMENT"
        fun newInstance(isFavorite: Boolean = false): CatsFragment =
            CatsFragment().apply { arguments = bundleOf(IS_FAVORITE_FRAGMENT to isFavorite) }

        private fun Bundle?.isFavoriteFragment(): Boolean =
            this?.getBoolean(IS_FAVORITE_FRAGMENT) ?: false
    }

    private var _binding: FragmentCatsListBinding? = null
    private val binding get() = _binding!!

    private var catsAdapter: CatsAdapter? = null

    private var isLoading = false
    private var currentPage = 0
    private var downloadID: Long? = null
    private var dm: DownloadManager? = null
    private var isDownloadPermission = false
    private var isFavorite = false

    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentCatsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        requireActivity().unregisterReceiver(broadcastReceiver)
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().registerReceiver(
            broadcastReceiver,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
        dm = requireActivity().getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        isFavorite = arguments.isFavoriteFragment()

        // Получение данных для отображения
        when {
            isFavorite -> viewModel.getFavoriteCats()
            else -> viewModel.getPageCatsNetwork(currentPage)
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            checkPermissionReadStorage()
        } else {
            isDownloadPermission = true
        }
        with(binding) {
            viewModel.cats.observe(viewLifecycleOwner) { cats ->
                if (!isFavorite) {
                    cats.loadItemsNetwork()
                }
            }
            viewModel.favorites.observe(viewLifecycleOwner) { favorite ->
                if (isFavorite) {
                    favorite.loadItemsFavorite()
                } else {
                    Toast.makeText(
                        context,
                        getString(R.string.count_favorite, favorite.size.toString()),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            viewModel.errorMessage.observe(viewLifecycleOwner) {
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            }
            viewModel.loading.observe(viewLifecycleOwner) {
                swipeRefresh.isRefreshing = it
            }


            catsAdapter = CatsAdapter(
                mutableListOf(),
                { item ->
                    if (item.isFavorites) {
                        viewModel.deleteFavorite(item)
                    } else {
                        viewModel.addFavorite(item)
                    }
                    item.apply {
                        isFavorites = !this.isFavorites
                    }

                    catsAdapter?.addFavorite(item)
                },
                { item ->
                    if (item.imageUrl.toHttpUrlOrNull() == null) {
                        Toast.makeText(context, R.string.url_error, Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        val file = File(
                            getRootDirPath(),
                            item.imageUrl.substringAfterLast("/")
                        )
                        if (isDownloadPermission) {
                            file.downloadFile(item.imageUrl, item.id)
                        } else {
                            showPermissionRequestExplanation()
                        }
                    }
                }
            )
            if (!isFavorite) {
                swipeRefresh.setOnRefreshListener {
                    isLoading = true
                    currentPage++
                    viewModel.getPageCatsNetwork(currentPage)
                }
            }

            catsList.apply {
                val manager = LinearLayoutManager(context).apply {
                    layoutManager = LinearLayoutManager(activity).apply {
                        orientation = LinearLayoutManager.VERTICAL
                    }
                    adapter = catsAdapter
                }
                layoutManager = manager
                if (!isFavorite) {
                    addOnScrollListener(object :
                        PaginationListener(manager, 10) {
                        override fun scrollStarted() {}

                        override fun isLastItem(): Boolean =
                            currentPage == (catsAdapter?.itemCount ?: 10) / 10

                        override fun isLoading(): Boolean = isLoading

                        override fun loadMoreItems() {
                            isLoading = true
                            currentPage++
                            catsAdapter?.addLoading()
                            viewModel.getPageCatsNetwork(currentPage)
                        }
                    })
                }
            }
        }
    }

    /**
     * Событие при получении новых котиков из интернета
     */
    private fun List<CatResponse>.loadItemsNetwork() {
        with(binding) {
            if (this@loadItemsNetwork.isEmpty()) {
                emptyCats.visibility = View.VISIBLE
                emptyCats.text = getString(R.string.empty_cats)
            } else {
                emptyCats.visibility = View.GONE
            }
            isLoading = false
            catsAdapter?.removeLoading()
            swipeRefresh.isRefreshing = false
            val items = this@loadItemsNetwork.map {
                AdapterItem.Cats(
                    it.id,
                    it.url,
                    it.getRectangle(Constants.ImageConst.MAX_SIZE)
                )
            }
            catsAdapter?.setCats(items)
        }
    }

    /**
     * Событие при получении котиков из избарнных
     */
    private fun List<CatFavorite>.loadItemsFavorite() {
        with(binding) {
            if (this@loadItemsFavorite.isEmpty()) {
                emptyCats.visibility = View.VISIBLE
                emptyCats.text = getString(R.string.favorite_empty_cats)
            } else {
                emptyCats.visibility = View.GONE
            }
            isLoading = false
            catsAdapter?.removeLoading()
            swipeRefresh.isRefreshing = false
            val items = this@loadItemsFavorite.map {
                AdapterItem.Cats(
                    it.catId,
                    it.path,
                    it.getRectangle(Constants.ImageConst.MAX_SIZE),
                    isFavorites = true
                )
            }
            catsAdapter?.updateCats(items)
        }
    }

    /**
     * BroadcastReceiver для загрузки картинок
     */
    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            val id = intent!!.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE == action && downloadID == id) {
                val query = DownloadManager.Query()
                query.setFilterById(id)
                dm?.let {
                    val c: Cursor = it.query(query)
                    if (c.moveToFirst()) {
                        val columnIndex: Int = c.getColumnIndex(DownloadManager.COLUMN_STATUS)
                        when (c.getInt(columnIndex)) {
                            DownloadManager.STATUS_SUCCESSFUL -> {
                                Toast.makeText(
                                    context,
                                    R.string.download_success,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            DownloadManager.STATUS_FAILED, DownloadManager.STATUS_PENDING -> {
                                Toast.makeText(context, R.string.download_error, Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Проверка перимшинов на запись файлов
     */
    private fun checkPermissionReadStorage() {
        context?.let {
            when {
                isAllowPermissionWriteExternalStorage -> isDownloadPermission = true
                shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) ->
                    showPermissionRequestExplanation()
                else -> {
                    requestPermissionLauncher.launch(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                }
            }
        }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            permissions.entries.forEach {
                if (it.key == Manifest.permission.WRITE_EXTERNAL_STORAGE && it.value) {
                    isDownloadPermission = true
                }
            }
        }

    /**
     * Отображение тоста что необходимо предоствить доступ к записи файлов
     */
    private fun showPermissionRequestExplanation() {
        Toast.makeText(context, R.string.permission_write_external_storage, Toast.LENGTH_SHORT)
            .show()
    }

    /**
     * Есть ли доступ к записи файлов
     */
    private val isAllowPermissionWriteExternalStorage: Boolean
        get() = context?.let {
            ActivityCompat.checkSelfPermission(it, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        } == PackageManager.PERMISSION_GRANTED

    private fun File.downloadFile(link: String, name: String) {
        downloadID = dm?.enqueue(
            DownloadManager.Request(Uri.parse(link))
                .setAllowedNetworkTypes(
                    DownloadManager.Request.NETWORK_MOBILE or
                            DownloadManager.Request.NETWORK_WIFI
                )
                .setDestinationUri(Uri.fromFile(this))
                .setTitle(name)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        )
    }

    /**
     * Получить путь где будет хранится скаченный файл
     */
    private fun getRootDirPath(): String {
        return File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS
            ), Constants.ImageConst.DIR_NAME
        ).absolutePath
    }

    /**
     * Получить размеры картинки кратно maxSize
     */
    private fun CatResponse.getRectangle(maxSize: Int): ImageParams {
        val maxParam = max(this.height, this.width)
        val minParam = min(this.height, this.width)

        val factor = maxSize * minParam / maxParam
        val height = when {
            this.height > this.width -> maxSize
            else -> factor
        }

        val width = when {
            this.width > this.height -> maxSize
            else -> factor
        }
        return ImageParams(height, width)
    }

    private fun CatFavorite.getRectangle(maxSize: Int): ImageParams {
        val maxParam = max(this.height, this.width)
        val minParam = min(this.height, this.width)

        val factor = maxSize * minParam / maxParam
        val height = when {
            this.height > this.width -> maxSize
            else -> factor
        }

        val width = when {
            this.width > this.height -> maxSize
            else -> factor
        }
        return ImageParams(height, width)
    }
}