package ru.mytestapp.catlist.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.mytestapp.catlist.data.entities.CatFavorite
import ru.mytestapp.catlist.data.network.response.CatResponse
import ru.mytestapp.catlist.repository.DbRepository
import ru.mytestapp.catlist.repository.NetworkRepository
import ru.mytestapp.catlist.ui.fragment.AdapterItem
import ru.mytestapp.catlist.ui.fragment.AdapterItem.Cats.Companion.mapToFavorites


class MainViewModel(
    private val dbRepository: DbRepository,
    private val networkRepository: NetworkRepository
) : ViewModel() {

    val cats = MutableLiveData<List<CatResponse>>()
    val favorites = MutableLiveData<List<CatFavorite>>()

    val loading = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<String>()
    var job: Job? = null

    fun getPageCatsNetwork(page: Int) {
        job = CoroutineScope(Dispatchers.IO).launch {
            cats.postValue(networkRepository.getCats(page))
            loading.postValue(false)
        }
    }

    fun getFavoriteCats() {
        job = CoroutineScope(Dispatchers.IO).launch {
            favorites.postValue(dbRepository.getFavorites())
            loading.postValue(false)
        }
    }

    fun addFavorite(cat: AdapterItem.Cats) {
        job = CoroutineScope(Dispatchers.IO).launch {
            dbRepository.addFavorite(cat.mapToFavorites())
            favorites.postValue(dbRepository.getFavorites())
            loading.postValue(false)
        }
    }


    fun deleteFavorite(cat: AdapterItem.Cats) {
        job = CoroutineScope(Dispatchers.IO).launch {
            dbRepository.deleteFavorite(cat.id)
            favorites.postValue(dbRepository.getFavorites())
            loading.postValue(false)
        }
    }
}