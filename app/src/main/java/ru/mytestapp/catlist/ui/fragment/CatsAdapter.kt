package ru.mytestapp.catlist.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import ru.mytestapp.catlist.R
import ru.mytestapp.catlist.databinding.ItemCatBinding
import ru.mytestapp.catlist.databinding.ItemLoadingBinding
import ru.mytestapp.catlist.ui.fragment.TypeItem.Companion.getType


class CatsAdapter(
    private val cats: MutableList<AdapterItem> = mutableListOf(),
    private val favorites: (AdapterItem.Cats) -> Unit,
    private val download: (AdapterItem.Cats) -> Unit
) : RecyclerView.Adapter<CatsAdapter.ItemViewHolder>() {

    private var isLoaderVisible = false

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ItemViewHolder =
        when (viewType.getType()) {
            TypeItem.LOAD -> ItemViewHolder.ViewHolderLoad(
                ItemLoadingBinding.inflate(
                    LayoutInflater.from(viewGroup.context),
                    viewGroup,
                    false
                )
            )
            TypeItem.NORMAL -> ItemViewHolder.ViewHolderCats(
                ItemCatBinding.inflate(
                    LayoutInflater.from(viewGroup.context),
                    viewGroup,
                    false
                )
            )
        }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        when (holder) {
            is ItemViewHolder.ViewHolderCats -> holder.bind(
                cats[position],
                favorites,
                download
            )
            is ItemViewHolder.ViewHolderLoad -> holder.bind()

        }
    }

    override fun getItemCount(): Int = cats.size

    override fun getItemViewType(position: Int): Int =
        when {
            isLoaderVisible && position == cats.size - 1 -> TypeItem.LOAD.type
            else -> TypeItem.NORMAL.type
        }

    @SuppressLint("NotifyDataSetChanged")
    fun setCats(catsList: List<AdapterItem.Cats>) {
        cats.addAll(catsList)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateCats(catsList: List<AdapterItem.Cats>) {
        cats.clear()
        cats.addAll(catsList)
        notifyDataSetChanged()
    }

    fun addFavorite(cat: AdapterItem.Cats) {
        cats.firstOrNull { it is AdapterItem.Cats && it.id == cat.id && it.imageUrl == it.imageUrl }
            ?.let {
                if (it is AdapterItem.Cats) {
                    it.isFavorites = cat.isFavorites
                }
            }
        notifyDataSetChanged()
    }

    fun addLoading() {
        isLoaderVisible = true
        cats.add(AdapterItem.Load)
        notifyItemInserted(cats.size - 1)
    }

    fun removeLoading() {
        isLoaderVisible = false
        val position: Int = cats.size - 1
        if (position > 0) {
            val item: AdapterItem = cats[position]
            if (item is AdapterItem.Load) {
                cats.removeAt(position)
                notifyItemRemoved(position)
            }
        }
    }

    sealed class ItemViewHolder(itemView2: View) : RecyclerView.ViewHolder(itemView2) {
        class ViewHolderCats(private val binding: ItemCatBinding) :
            ItemViewHolder(binding.root) {
            fun bind(
                item: AdapterItem,
                favorites: (AdapterItem.Cats) -> Unit,
                download: (AdapterItem.Cats) -> Unit
            ) =
                with(itemView) {
                    with(binding) {
                        if (item is AdapterItem.Cats) {
                            context?.let {
                                process.startAnimation(it, itemView)
                            }

                            Picasso.get()
                                .load(item.imageUrl)
                                .resize(item.imageParams.width, item.imageParams.height)
                                .into(binding.photoCat, object : Callback {
                                    override fun onSuccess() {
                                        process.visibility = View.GONE
                                        process.clearAnimation()
                                    }

                                    override fun onError(e: Exception?) {
                                        process.visibility = View.GONE
                                        process.clearAnimation()
                                    }
                                })
                            val iconFavorite = when {
                                item.isFavorites -> ContextCompat.getDrawable(
                                    context,
                                    R.drawable.ic_favorite_added
                                )
                                else -> ContextCompat.getDrawable(
                                    context,
                                    R.drawable.ic_favorite_add
                                )
                            }
                            addFavorite.setImageDrawable(iconFavorite)

                            addFavorite.setOnClickListener { favorites(item) }
                            catDownload.setOnClickListener { download(item) }

                        }
                    }
                }


        }

        class ViewHolderLoad(private val binding: ItemLoadingBinding) :
            ItemViewHolder(binding.root) {
            fun bind() =
                with(itemView) {
                    context?.let {
                        binding.process.startAnimation(it, itemView)
                    }
                }
        }

        fun AppCompatImageView.startAnimation(context: Context, itemView: View) {
            AnimatedVectorDrawableCompat.create(context, R.drawable.avd_anim_progress)?.let {
                setImageDrawable(it)
                it.registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {
                    override fun onAnimationStart(drawable: Drawable) {
                        super.onAnimationStart(drawable)
                    }

                    override fun onAnimationEnd(drawable: Drawable) {
                        super.onAnimationEnd(drawable)
                        itemView.post { it.start() }
                    }
                })
                it.start()
            }
        }
    }
}

