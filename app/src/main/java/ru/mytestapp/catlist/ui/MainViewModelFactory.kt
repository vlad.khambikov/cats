package ru.mytestapp.catlist.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.mytestapp.catlist.repository.DbRepository
import ru.mytestapp.catlist.repository.NetworkRepository
import javax.inject.Inject

class MainViewModelFactory @Inject constructor(
    val dbRepository: DbRepository,
    val networkRepository: NetworkRepository
) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(dbRepository, networkRepository) as T
    }
}