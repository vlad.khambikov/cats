package ru.mytestapp.catlist.ui.fragment

enum class TypeItem(val type: Int) {
    LOAD(0),
    NORMAL(1);

    companion object {
        fun Int.getType(): TypeItem =
            when {
                this == 0 -> LOAD
                else -> NORMAL
            }
    }
}