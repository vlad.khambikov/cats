package ru.mytestapp.catlist.ui.fragment

data class ImageParams(val height:Int, val width:Int)
