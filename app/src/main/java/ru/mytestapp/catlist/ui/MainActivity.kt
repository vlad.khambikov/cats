package ru.mytestapp.catlist.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import ru.mytestapp.catlist.R
import ru.mytestapp.catlist.app.CatsApplication.Companion.appComponent
import ru.mytestapp.catlist.databinding.ActivityMainBinding
import ru.mytestapp.catlist.ui.fragment.CatsFragment
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var vmFactory: MainViewModelFactory
    private lateinit var vm: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(R.layout.activity_main)
        applicationContext.appComponent.inject(this)
        vm = ViewModelProvider(this, vmFactory)[MainViewModel::class.java]

        val toolBarView: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolBarView)
        val fragment = CatsFragment.newInstance()
        supportFragmentManager.beginTransaction()
            .add(R.id.content, fragment, fragment.javaClass.name)
            .addToBackStack(this.javaClass.name)
            .commit()
        bindToolBar(getString(R.string.toolbar_name_base), false)

        toolBarView.setNavigationOnClickListener {
            val fragment = CatsFragment.newInstance()
            supportFragmentManager.beginTransaction()
                .replace(R.id.content, fragment, fragment.javaClass.name)
                .addToBackStack(this.javaClass.name)
                .commit()
            bindToolBar(getString(R.string.toolbar_name_base), false)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.favorite_menu -> {
                val favorite = CatsFragment.newInstance(true)
                supportFragmentManager.beginTransaction()
                    .replace(R.id.content, favorite, favorite.javaClass.name)
                    .addToBackStack(this.javaClass.name)
                    .commit()
                bindToolBar(getString(R.string.toolbar_name_favorites), true)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun bindToolBar(titleName: String, isFavorites: Boolean) {
        with(binding) {
            toolbar.title = titleName
            if (isFavorites) {
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
            } else {
                supportActionBar?.setDisplayHomeAsUpEnabled(false)
            }
        }
    }
}