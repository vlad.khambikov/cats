package ru.mytestapp.catlist.app

import android.content.Context
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import ru.mytestapp.catlist.di.AppComponent
import ru.mytestapp.catlist.di.DaggerAppComponent
import ru.mytestapp.catlist.di.modules.AppModule
import ru.mytestapp.catlist.di.modules.DataModule
import ru.mytestapp.catlist.di.modules.DomainModule
import ru.mytestapp.catlist.di.modules.NetworkCatsModule

class CatsApplication : DaggerApplication() {

    val appComponent by lazy {
        DaggerAppComponent.builder()
            .application(this@CatsApplication)
            .appModule(AppModule())
            .domainModule(DomainModule())
            .dataModule(DataModule())
            .netWorkModule(NetworkCatsModule())
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        appComponent
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return appComponent.also { it.inject(this) }
    }

    companion object {
        val Context.appComponent: AppComponent
            get() = when (this) {
                is CatsApplication -> appComponent
                else -> this.applicationContext.appComponent
            }
    }
}